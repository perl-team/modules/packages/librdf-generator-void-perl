Source: librdf-generator-void-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 perl,
 debhelper (>= 10~),
 dh-buildinfo,
 libossp-uuid-perl,
 libmoose-perl,
 librdf-trine-perl,
 liburi-perl,
 libaliased-perl,
 libtest-rdf-perl (>= 1.10),
 libapp-cmd-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.1.3
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/librdf-generator-void-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/librdf-generator-void-perl
Homepage: https://metacpan.org/module/RDF::Generator::Void
Testsuite: autopkgtest-pkg-perl

Package: librdf-generator-void-perl
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${perl:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Enhances: ${cdbs:Enhances}
Description: generate VoID descriptions based on data in an RDF model
 Resource Description Framework (RDF) is a standard model for data
 interchange on the Web.
 .
 VoID is an RDF Schema vocabulary for expressing metadata about RDF
 datasets, intended as a bridge between the publishers and users of RDF
 data. More info at <http://www.w3.org/TR/void/>.
 .
 RDF::Generator::Void takes a RDF::Trine::Model object as input to the
 constructor, and based on the data in that model as well as data
 supplied by the user, it creates a new model with a VoID description of
 the data in the model.
